# Chpt5

In this repository, there are the tables with information regarding Chapter 5 "Time, organ, and species interactions define doublesex expression in Nasonia" of the thesis "The role of doublesex in shaping temporal, spatial and species-specific sex differentiation in Nasonia wasps"

Supplementary Table 1: primers used in PCR-mediated amplification of Trichomalopsis sarcophagae Ws1 locus.

Supplementary Table 2: primers used in qPCR-mediated gene expression analysis.
